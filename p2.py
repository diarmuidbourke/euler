def fib(x, y):
    return [x, y, x+y]

s = 2
x = 1
y = 2
while x < 4000001:
    x, y, z = fib(x, y)
    if z % 2 == 0:
        s += z
    x = y
    y = z
print(s)
