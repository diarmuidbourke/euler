# Smallest number evenly divisible by 1 to 20
# Properties: Will be even, last digit zero.

def invalid(number):
    for i in xrange(10, 21):
        if number % i != 0:
            return True
    return False

number = 20
while True:
    if invalid(number):
        number += 20
    else:
        break

print(number)

