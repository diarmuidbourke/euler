def sum_of_squares(n):
    return sum([x*x for x in xrange(1, n+1)])

def square_of_sum(n):
    return sum(xrange(1, n+1))**2


sqsum = square_of_sum(100)
print(sqsum)
sumsq = sum_of_squares(100)
print(sumsq)
difference = sqsum - sumsq
print(difference)
