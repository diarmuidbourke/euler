# Largest palindrome from product of two three digit numbers
# Largest possible number is 999 * 999 = 998001
# Largest palindrome is ... 997799 ?
# Largest with two three digit factors is ...
first = xrange(100, 1000)
second = xrange(100, 1000)
print(max([x*y for x in first for y in second if str(x*y) == str(x*y)[::-1]]))
