def primes(limit):
    # We know that all the multiples of two aren't prime, so we can use a sieve
    # half the size that Eratosthenes says to use
    sieve = [True] * (limit/2)
    # print("Sieve length is %d" % len(sieve))

    # 3 is our first prime number, so start here.
    # Iterate over a list from 3 to the square root of n in steps of 2
    # We can do this because a prime number + 1 is not prime because it's even.
    # and we only go to the square root of n, because we're going to invalidate
    # all multiples of any prime we find and the remaining . ie: our biggest prime is between 3 and
    # the square root of n. 
    for i in xrange(3, int(limit**0.5)+1, 2): 
        if sieve[i/2]: # Use the next unmarked number
            # Mark every multiple of i as not prime
            # This uses pythons list slice assignment
            # We create a list of Falses which has a length equal to the number
            # of multiples we want to invalidate.
            sieve[i*i/2::i] = [False] * ( (limit-i * i-1) / (2 * i) + 1 )
    # We now return the index of each positive entry in the sieve
    # multiplying by 2 and adding 1 as we're using a half sieve.
    return [2] + [2*i+1 for i in xrange(1, limit/2) if sieve[i]]


def largest_prime_factors(x):
    prime_factors = []
    for prime in primes(int(x**0.5)):
        if x % prime == 0:
            prime_factors.append(prime)
    return prime_factors


#print(len(primes(int(600851475143**0.5))))
print(largest_prime_factors(600851475143))
#print(largest_prime_factors(60000))

